import numpy as np

np.random.seed(123)  # for reproducibility

from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Input, Flatten, Lambda
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
from keras import backend as K
import tensorflow as tf

X = np.array([[1, 1]])
# Y = np.array([[1], []])

model = Sequential()
model.add(Dense(3, input_shape=(2,), activation='linear', kernel_initializer="Ones"))
model.add(Dense(1, activation='linear', kernel_initializer="Ones"))
# model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# model.fit(X, Y, batch_size=32, epochs=1)
# get_1rd_layer_output = K.function([model.layers[0].input],
#                                   [model.layers[1].output])

input_layer = Input(shape=(2,), name="General_Input")
model_1_layer = model(input_layer)
last_layer = Lambda(lambda x: x ** 2)(model_1_layer)
model2 = Model(inputs=input_layer, outputs=last_layer)

get_1rd_layer_output = K.function([model2.layers[0].input],
                                  [model2.layers[2].output])

print("OUTPUT: " + str(get_1rd_layer_output([X])))
print("Finish")


