import numpy as np

np.random.seed(1)  # for reproducibility

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Input, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
from keras import backend as K

K.set_image_dim_ordering('th')
# keras.optimizers.SGD(lr=0.000001, momentum=0.0, decay=0.0, nesterov=False)

X = np.array([[1, 1], [0, 0], [0, 1], [1, 0]])
Y = np.array([1, 0, 0, 0])

model = Sequential()
model.add(Dense(64, input_shape=(2,), activation='relu'))
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(X, Y, batch_size=32, epochs=30000)

predicted_values = model.predict(X)
print("Str: " + str(predicted_values))
