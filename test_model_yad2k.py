import numpy as np

np.random.seed(123)  # for reproducibility

from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Input, Flatten, Lambda
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
from keras import backend as K
import tensorflow as tf


def lambda_func(some_input):
    print("!!!" + str(some_input))
    return some_input**2


X = np.array([[1, 1]])
Y = np.array([[1, 1, 1, 1]])


input_layer = Input(shape=(2,), name="General_Input")
middle_layer = Dense(4, activation='relu')(input_layer)
model_loss = Lambda(lambda_func)(middle_layer)

model = Model(inputs=input_layer, outputs=model_loss)
model.compile(optimizer='adam', loss="categorical_crossentropy")
model.fit(X, Y, epochs=1)

print("--== Finished ==--")
